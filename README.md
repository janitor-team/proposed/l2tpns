# Layer 2 tunneling protocol network server (LNS)

* Mailing list: <https://lists.ffdn.org/wws/info/l2tpns>
* Code: <https://code.ffdn.org/l2tpns/l2tpns>
