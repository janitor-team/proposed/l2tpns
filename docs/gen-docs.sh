#!/bin/sh

# Source documentation in markdown lives in the src/ folder (html and manpages).
# This is what you should edit if you want to make changes to the documentation.
# From these sources, this script generates actual manpages and general
# documentation in html using pandoc.

if ! [ -x "$(command -v pandoc)" ]; then
    echo "Pandoc is missing, please install it first"
    exit 1
fi

# First we generate manpages

echo "Manpages generation …"

for src in src/man/*.md
    do
        pandoc -s -t man "$src" -o manpages/"$(basename "$src" .md)" && echo "$(basename "$src" .md) successfully built in docs/manpages directory" || echo "Unable to generate manpage from $src"
done

# We then generate the rest of the documentation

echo ""

echo "HTML generation …"


for src in src/html/*.md
    do
        pandoc -f markdown -t html "$src" > html/"$(basename "$src" .md)".html && echo "$(basename "$src" .md).html successfully built in docs/html directory" || echo "Unable to generate html from $src"
done

